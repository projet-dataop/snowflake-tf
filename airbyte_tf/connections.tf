resource "airbyte_connection" "projet" {
  data_residency                       = "eu"
  destination_id                       = airbyte_destination_snowflake.bronze.destination_id
  name                                 = "projet DataOps"
  non_breaking_schema_updates_behavior = "propagate_fully"
  source_id                            = airbyte_source_faker.faker.source_id
  status                               = "active"
  schedule = {
    schedule_type = "manual"
  }
}
