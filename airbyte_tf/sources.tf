resource "airbyte_source_faker" "faker" {
  configuration = {
    always_updated    = true
    count             = 1000
    parallelism       = 4
    records_per_slice = 1000
    seed              = -1
  }
  name         = "Origin"
  workspace_id = "72e22a80-77d0-46b7-9935-8daaa092429d"
}
