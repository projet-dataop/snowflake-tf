variable "snowflake_password" {
  description = "Airbyte password"
  type        = string
  sensitive   = true
}
variable "airbyte_instance_api" {
  description = "Airbyte instance API's base URL"
  type        = string
  sensitive   = true
}
