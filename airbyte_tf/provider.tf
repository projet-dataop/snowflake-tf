terraform {
  required_providers {
    airbyte = {
      source  = "airbytehq/airbyte"
      version = "0.5.0"
    }
  }
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/58055620/terraform/state/airbyte"
    lock_address   = "https://gitlab.com/api/v4/projects/58055620/terraform/state/airbyte/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/58055620/terraform/state/airbyte/lock"
  }
}
provider "airbyte" {
  server_url = var.airbyte_instance_api
}
