resource "airbyte_destination_snowflake" "bronze" {
  configuration = {
    credentials = {
      username_and_password = {
        password = var.snowflake_password
      }
    }
    disable_type_dedupe   = true
    host                  = "liaknou-zi03338.snowflakecomputing.com"
    role                  = "INGESTING"
    warehouse             = "INGESTION_WAREHOUSE"
    database              = "BRONZE"
    schema                = "PROJET_DATAOPS"
    username              = "AIRBYTE"
    retention_period_days = 1
  }
  name         = "projet DataOps (BRONZE)"
  workspace_id = "72e22a80-77d0-46b7-9935-8daaa092429d"
}
