terraform {
  required_providers {
    snowflake = {
      source  = "Snowflake-Labs/snowflake"
      version = "0.88.0"
    }
  }
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/58055620/terraform/state/default"
    lock_address   = "https://gitlab.com/api/v4/projects/58055620/terraform/state/default/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/58055620/terraform/state/default/lock"
  }
}
provider "snowflake" {
  account  = var.account_id
  user     = var.main_username
  password = var.main_password
  role     = "ACCOUNTADMIN"
}
