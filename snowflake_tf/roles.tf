### ROLES ###
resource "snowflake_role" "ingesting" {
  name    = "INGESTING"
  comment = "A role for ingesting data into the BRONZE layer."
}
resource "snowflake_role" "cleaning" {
  name    = "CLEANING"
  comment = "A role for transition from the BRONZE layer to SILVER."
}
### GRANTS ###
locals {
  role_to_main_db = {
    (snowflake_role.ingesting.name) = snowflake_database.DB["bronze"].name
    (snowflake_role.cleaning.name)  = snowflake_database.DB["silver"].name
  }
  role_to_main_wh = {
    (snowflake_role.ingesting.name) = snowflake_warehouse.ingestion_warehouse.name
    (snowflake_role.cleaning.name)  = "COMPUTE_WH"
  }
}
##### Warehouse privileges ######
resource "snowflake_grant_privileges_to_account_role" "grant_airbyte_ingesting_WH" {
  for_each          = local.role_to_main_wh
  depends_on        = [snowflake_role.ingesting, snowflake_role.cleaning]
  privileges        = ["USAGE"]
  account_role_name = each.key
  on_account_object {
    object_type = "WAREHOUSE"
    object_name = each.value
  }
}
##### Main Database privileges ######
resource "snowflake_grant_privileges_to_account_role" "grant_main_database" {
  for_each          = local.role_to_main_db
  depends_on        = [snowflake_role.ingesting, snowflake_role.cleaning]
  all_privileges    = true
  account_role_name = each.key
  on_account_object {
    object_type = "DATABASE"
    object_name = each.value
  }
}
resource "snowflake_grant_privileges_to_account_role" "grant_main_schema" {
  for_each          = local.role_to_main_db
  depends_on        = [snowflake_schema.projetDataOps, snowflake_role.cleaning, snowflake_role.ingesting]
  all_privileges    = true
  account_role_name = each.key
  on_schema {
    schema_name = "\"${each.value}\".\"${local.schema_name}\""
  }
}
resource "snowflake_grant_privileges_to_account_role" "grant_main_tables" {
  for_each          = local.role_to_main_db
  depends_on        = [snowflake_schema.projetDataOps, snowflake_role.cleaning, snowflake_role.ingesting]
  privileges        = ["ALL"]
  account_role_name = each.key
  on_schema_object {
    all {
      object_type_plural = "TABLES"
      in_schema          = "\"${each.value}\".\"${local.schema_name}\""
    }
  }
}
##### Secondary Database privileges #####
resource "snowflake_grant_privileges_to_account_role" "cleaning_bronze_table_read_only" {
  privileges        = ["SELECT"]
  account_role_name = snowflake_role.cleaning.name
  depends_on        = [snowflake_schema.projetDataOps, snowflake_role.cleaning]
  on_schema_object {
    all {
      object_type_plural = "TABLES"
      in_schema          = "\"${snowflake_database.DB["bronze"].name}\".\"${local.schema_name}\""
    }
  }
}

##### cleaning ######
# resource "snowflake_grant_privileges_to_account_role" "cleaning_grant_bronze_schema" {
#   privileges        = ["USAGE"]
#   account_role_name = snowflake_role.ingesting.name
#   on_schema {
#     schema_name = "\"${snowflake_database.DB["bronze"].name}\".\"${local.schema_name}\""
#   }
# }
