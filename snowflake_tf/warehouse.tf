### WAREHOUSES ###
resource "snowflake_warehouse" "ingestion_warehouse" {
  name           = "INGESTION_WAREHOUSE"
  warehouse_size = "X-SMALL"
  auto_suspend   = 300
  auto_resume    = true
}
