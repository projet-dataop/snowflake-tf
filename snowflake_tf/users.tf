### USERS ###
resource "snowflake_user" "airbyte" {
  name                 = "AIRBYTE"
  login_name           = "airbyte"
  comment              = "Service user for airbyte."
  password             = var.airbyte_password
  display_name         = "AIRBYTE"
  must_change_password = false
}
resource "snowflake_user" "dbt" {
  name                 = "DBT"
  login_name           = "dbt"
  comment              = "Service user for DBT."
  password             = var.dbt_password
  display_name         = "DBT"
  must_change_password = false
}
### GRANT ###
resource "snowflake_grant_account_role" "grant_airbyte_ingesting" {
  role_name = snowflake_role.ingesting.name
  user_name = snowflake_user.airbyte.name
}
resource "snowflake_grant_account_role" "grant_dbt_cleaning" {
  role_name = snowflake_role.cleaning.name
  user_name = snowflake_user.dbt.name
}
