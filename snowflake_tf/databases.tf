### DATABASES ###
locals {
  database_names = {
    bronze = "BRONZE"
    silver = "SILVER"
  }
  schema_name = "PROJET_DATAOPS"
}
resource "snowflake_database" "DB" {
  for_each                    = local.database_names
  name                        = each.value
  data_retention_time_in_days = 3
}
### SCHEMAS ###
resource "snowflake_schema" "projetDataOps" {
  for_each   = local.database_names
  depends_on = [snowflake_database.DB]
  database   = each.value
  name       = local.schema_name
}
