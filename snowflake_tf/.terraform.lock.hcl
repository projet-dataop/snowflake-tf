# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/snowflake-labs/snowflake" {
  version     = "0.88.0"
  constraints = "0.88.0"
  hashes = [
    "h1:L5dQuGBllIjIk2iKOR15S49MAN7wHT68du+rlktuRW0=",
    "zh:0c9e1e0596ca77cc03f911bc2d164a72a61ed40ed79956e5624fd6d054eefac9",
    "zh:102b210ff3b464aea4992381fc63bd46ac92b38d85c1ce0bdca910604137b81e",
    "zh:1182e368736807d4688a1483676917dbfb1400cfba1aa61845e18a9019660b8f",
    "zh:12ad9acfaf91351799063c328d560b299a05e015af44d3c7189bfedc731a033a",
    "zh:57e349019ecd59b0899bc2f2bd8c5f21d38f4d53ec2e84a659e5e1438e4d30c9",
    "zh:8e03b5c2a7d16feb98b3b4c0de6270d14a649e9ccb7bcc13bbeac0960036f68e",
    "zh:c7e3c8e45eb64ec3d25760f0f664d449b57c1a3d07c86f198cfb1aa69602464a",
    "zh:d297c8338c39b84811cc6c9161da36f1bdd7ab71d9b48e15f37b734b4acd02c0",
    "zh:e7bec6a0fd8aed7771898b8d4d3c9fa4e1974da15ae52cff3410ffdbc1e6e245",
    "zh:ebd3858e0fabc5d3d8586a6446a29375241e776429af41490753b7171ef6f5f1",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:f952e9bdd890f61d2f33ab15c10641dbcca0fc3735debd9ee5d1562fa3340582",
  ]
}
