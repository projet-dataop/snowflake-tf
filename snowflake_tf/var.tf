variable "account_id" {
  description = "Snowflake account"
  type        = string
  sensitive   = true
}
variable "main_username" {
  description = "Snowflake username"
  type        = string
  sensitive   = true
}
variable "main_password" {
  description = "Snowflake password"
  type        = string
  sensitive   = true
}
variable "airbyte_password" {
  description = "password for AIRBYTE User"
  type        = string
  sensitive   = true
}
variable "dbt_password" {
  description = "Password for DBT user"
  type        = string
  sensitive   = true
}
