# Projet DATAOPS - config Terraform pour snowflake

## Schema Snowflake

![schéma snowflake][schema]

## Explorations

- `for_each`, `depends_on` (pas recommendé mais utilisé car plus simple que les modules),
- `.tfvars` pour les variables plutôt que le `init_var.sh`,
- Gestion de droits plus fine,
- Compte de service pour terraform.
- Backend HTTP pour gestion d'état via gitlab.
- CI/CD avancée [cf. #CI/CD](#cicd)

## CI/CD

Ce Yaml est peuplé grace au [template officiel de gitlab][ci] et modifié à partir de [ce tutoriel][tuto] et de mes compétences.

## AIRBYTE

### Instalation

Car nous somme dans le cadre d'un projet de découverte et que l'instance est sur mon ordinateur personnel (déjà protégé). J'ai pris la libertée de ne pas utiliser d'authentification pour airbyte en modifiant le `.env` tel que le veux la documentation.

### Modifications snowflake

Airbyte ~~est très capricieux~~ à une très mauvaise gestion des droits. Donc nous [sommes obligés][owner] de donner tout les droit pour les DB et Schéma créés.

> Il aurait été possible d'enlever le for_each des grants pour les DB et Schema, mais par manque de temps et crainte de retrouver le problème pour DBT, j'ai modifier les privilèges pour les 2 users.

### Terraform

Comme l'instance est installé uniquement en local et que je n'ai pas particulièrement envie de me prendre la tête à installer des runners gitlab sur mon ordinateur. J'ai désactivé les jobs concernants le déploiement/cleanup de airbyte.

Voici cependant une preuve que tout fonctionne bien:
![airbyte output][]

[//]: <> (LINKS.)
[schema]: docs/schema.jpg
[ci]: https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform.latest.gitlab-ci.yml
[tuto]: https://spacelift.io/blog/gitlab-terraform
[owner]: https://discuss.airbyte.io/t/why-does-the-snowflake-connector-need-the-ownership-role/785/2
[airbyte output]: docs/airbyte_output.png
